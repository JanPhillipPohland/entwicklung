﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;


public class GesturesManager : MonoBehaviour
{

    Controller controller;
    Hand left, right;
    Canvas dayCanvas;
    public bool hasSwipedUp = false;
    bool hasSwipedLeft = false;
    bool hasSwipedRight = false;
    bool hasPinched = false;

    // Use this for initialization
    void Start() {
        controller = new Controller();
        dayCanvas = GameObject.Find("TagCanvas").GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update() {
        left = null;
        right = null;
        Frame frame = controller.Frame();
        foreach (Hand h in frame.Hands) {
            if (h.IsLeft) {
                left = h;
            }

            if (h.IsRight) {
                right = h;
            }
        }

        if (!hasSwipedUp && !hasSwipedRight && !hasSwipedLeft && (!hasPinched && dayCanvas.enabled)) {
            SwipeUp();
            SwipeRight();
            SwipeLeft();
            Pinch();
        }
    }

    void Pinch() {
        if (left != null && right != null && left.IsPinching() && right.IsPinching()) {
            Vector3 leftPinchPos = left.GetPinchPosition();
            Vector3 rightPinchPos = right.GetPinchPosition();

            float dis = Vector3.Distance(leftPinchPos, rightPinchPos);
            if (dis < 30.0f) {
                Vector leftVel = left.PalmVelocity;
                Vector rightVel = right.PalmVelocity;

                if (leftVel.x < -30.0f && rightVel.x > 30.0f) {
                    hasPinched = true;
                    SwipeDelay();
                    GameObject.Find("UIManager").GetComponent<UIManager>().NewTermin();
                    GameObject.Find("MenuManager").GetComponent<MenuManager>().HideMainOptions();
                    GameObject.Find("MenuManager").GetComponent<MenuManager>().ShowNewOptions();

                }
            }
        }
    }

    void SwipeUp() {
        if (dayCanvas.enabled) {
            if (right != null) {
                Vector velocity = right.PalmVelocity;

                if (velocity.y < -480.0f && !hasSwipedUp) {
                    hasSwipedUp = true;

                    dayCanvas.GetComponent<Rigidbody>().AddForce(0.0f, 1800.0f, 0.0f, ForceMode.Impulse);
                    SwipeCanvasMovement();
                    SwipeDelay();
                }
            }
        }
    }

    void SwipeLeft() {
        if (dayCanvas.enabled) {
            if (left != null) {
                Vector velocity = left.PalmVelocity;

                if (velocity.x < -550.0f && !hasSwipedLeft) {
                    hasSwipedLeft = true;

                    GameObject.Find("UIManager").GetComponent<UIManager>().ChangeDay("left");
                    SwipeDelay();
                }
            }
        }
    }

    void SwipeRight() {
        if (dayCanvas.enabled) {
            if (left != null) {
                Vector velocity = left.PalmVelocity;

                if (velocity.x > 550.0f && !hasSwipedRight) {
                    hasSwipedRight = true;

                    GameObject.Find("UIManager").GetComponent<UIManager>().ChangeDay("right");
                    SwipeDelay();
                }
            }
        }
    }

    void SwipeDelay() {
        if (!hasSwipedRight) {
            StartCoroutine(SetSwipeFalse(2.0f));
        } else if (hasSwipedRight || hasSwipedLeft || hasPinched) {
            StartCoroutine(SetSwipeFalse(.8f));
        }
    }

    void SwipeCanvasMovement() {
        StartCoroutine(WaitAndMoveCanvas(0.5f));
    }

    IEnumerator SetSwipeFalse(float time) {
        yield return new WaitForSeconds(time);
        hasSwipedUp = false;
        hasSwipedLeft = false;
        hasSwipedRight = false;
        hasPinched = false;
    }

    IEnumerator WaitAndMoveCanvas(float time) {
        yield return new WaitForSeconds(time);

        if (hasSwipedUp) {
            if (dayCanvas.transform.position.y > 500.0f) {
                dayCanvas.GetComponent<Rigidbody>().velocity = Vector3.zero;

                GameObject.Find("UIManager").GetComponent<UIManager>().HideDay();
                GameObject.Find("UIManager").GetComponent<UIManager>().Setinteractable();
            }
        }
    }
}

