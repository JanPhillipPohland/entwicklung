﻿using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{

    public GameObject prefabTermin;
    public GameObject prefabNewTermin;
    Canvas dayCanvas;
    Canvas handCanvas;
    GesturesManager gesturesManager;
    Canvas monthCanvas;
    Text dayTitle;

    string bName;

    Vector3 dayCanvasPosition;

    public List<GameObject> instantiatedGameObjects = new List<GameObject>();
    List<Datum> datenList = new List<Datum>();
    Datum[] datenArray = new Datum[31];

    public GameObject menuManager;

    GameObject saveButton;

    int startHours;
    int endHours;
    int startMinutes;
    int endMinutes;

    public bool customColor;

    void Start() {
        GameObject[] d = GameObject.FindGameObjectsWithTag("Datum");
        foreach (GameObject g in d) {
            datenList.Add(g.GetComponent<Datum>());
        }

        datenList.Sort(new SortDaten());
        datenArray = datenList.ToArray();

        dayCanvas = GameObject.Find("TagCanvas").GetComponent<Canvas>();
        dayCanvasPosition = dayCanvas.transform.position;

        monthCanvas = GameObject.Find("MonatCanvas").GetComponent<Canvas>();

        dayTitle = dayCanvas.transform.Find("TagMonatPanel").transform.Find("Text").GetComponent<Text>();
        dayTitle.color = Color.white;
        dayTitle.fontSize = 500;
        dayTitle.fontStyle = FontStyle.Italic;

        dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable = false;

        handCanvas = GameObject.Find("HandCanvas").GetComponent<Canvas>();

        gesturesManager = GameObject.Find("Gestures").GetComponent<GesturesManager>();

        saveButton = dayCanvas.transform.Find("SaveButtonPopUp").gameObject;
        saveButton.GetComponent<Button>().interactable = false;

        if (dayCanvas != null && dayCanvas.isActiveAndEnabled) {
            dayCanvas.enabled = false;
        }

        handCanvas.enabled = false;
    }

    public void OpenDay() {
        dayCanvas.transform.position = dayCanvasPosition;
        if (!dayCanvas.enabled) {
            dayCanvas.enabled = true;
        }

        string text = null;
        if (EventSystem.current.currentSelectedGameObject.name != null) {
            string n = EventSystem.current.currentSelectedGameObject.name;
            Button[] buttons = monthCanvas.GetComponentsInChildren<Button>();

            foreach (Button b in buttons) {
                if (n.Equals(b.name)) {
                    text = n;
                }
            }
        }

        dayTitle.text = text + " Juli 2018";
        bName = EventSystem.current.currentSelectedGameObject.name;
        ShowTermine();
        Setinteractable();
    }

    public void HideDay() {
        dayCanvas.enabled = false;

        ClearTermine();
    }

    public void ChangeDay(string dir) {
        dayCanvas.transform.position = dayCanvasPosition;
        if (!dayCanvas.enabled) {
            dayCanvas.enabled = true;
        }

        ClearTermine();

        for (int i = 0; i < datenArray.Length; i++) {
            if (datenArray[i].name.Equals(bName) && !datenArray[i].name.Equals("31") && dir.Equals("left")) {
                dayTitle.text = datenArray[i + 1].name + " Juli 2018";
                bName = datenArray[i + 1].name;
                break;
            } else if (datenArray[i].name.Equals(bName) && datenArray[i].name.Equals("31") && dir.Equals("left")) {
                dayTitle.text = datenArray[0].name + " Juli 2018";
                bName = datenArray[0].name;
                break;
            }

            if (datenArray[i].name.Equals(bName) && !datenArray[i].name.Equals("01") && dir.Equals("right")) {
                dayTitle.text = datenArray[i - 1].name + " Juli 2018";
                bName = datenArray[i - 1].name;
                break;
            } else if (datenArray[i].name.Equals(bName) && datenArray[i].name.Equals("01") && dir.Equals("right")) {
                dayTitle.text = datenArray[datenArray.Length - 1].name + " Juli 2018";
                bName = datenArray[datenArray.Length - 1].name;
                break;
            }
        }

        ShowTermine();
    }

    public void Setinteractable() {
        Button[] buttons = monthCanvas.GetComponentsInChildren<Button>();

        foreach (Button b in buttons) {
            if (b.interactable) {
                b.interactable = false;
            } else {
                b.interactable = true;
            }
        }

        if (dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable) {
            dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable = false;
        }
    }

    public void MyCoroutine() {
        OpenDay();
        StartCoroutine(CloseButtonInteractable(1.5f));
    }

    public void ShowHandCanvas() {
        if (dayCanvas.enabled && !menuManager.GetComponent<MenuManager>().optionsOpened && !menuManager.GetComponent<MenuManager>().presetsOpened && !menuManager.GetComponent<MenuManager>().timeOpened) {
            handCanvas.enabled = true;
            menuManager.GetComponent<MenuManager>().ShowMainOptions();
        } else if (dayCanvas.enabled && menuManager.GetComponent<MenuManager>().optionsOpened && !menuManager.GetComponent<MenuManager>().presetsOpened && !menuManager.GetComponent<MenuManager>().timeOpened) {
            handCanvas.enabled = true;
            menuManager.GetComponent<MenuManager>().HideMainOptions();
            menuManager.GetComponent<MenuManager>().ShowNewOptions();
        } else if (dayCanvas.enabled && menuManager.GetComponent<MenuManager>().presetsOpened && !menuManager.GetComponent<MenuManager>().optionsOpened && !menuManager.GetComponent<MenuManager>().timeOpened) {
            handCanvas.enabled = true;
            menuManager.GetComponent<MenuManager>().HideMainOptions();
            menuManager.GetComponent<MenuManager>().ShowPresets();
        } else if (dayCanvas.enabled && menuManager.GetComponent<MenuManager>().timeOpened && !menuManager.GetComponent<MenuManager>().optionsOpened && !menuManager.GetComponent<MenuManager>().presetsOpened) {
            handCanvas.enabled = true;
            menuManager.GetComponent<MenuManager>().HideMainOptions();
            menuManager.GetComponent<MenuManager>().ShowTimeOptions();
        }
    }

    public void HideHandCanvas() {
        handCanvas.enabled = false;
    }

    public void ShowTermine() {
        string n = bName;

        foreach (Datum d in datenList) {
            if (d.name.Equals(n)) {
                if (d.termine.Count > 0) {
                    foreach (Termin termin in d.termine) {
                        if (termin != null) {
                            GameObject g = Instantiate(prefabTermin);
                            g.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = termin.GetColor();
                            g.transform.Find("TitelText").GetComponent<Text>().text = termin.GetTitel().ToString();
                            g.transform.Find("StartTimeText").GetComponent<Text>().text = termin.GetStart();
                            g.transform.Find("EndTimeText").GetComponent<Text>().text = termin.GetEnd();

                            g.transform.SetParent(dayCanvas.transform, false);
                            if (d.termine.IndexOf(termin) != 0) {
                                g.transform.position += new Vector3(0.0f, -80.0f * d.termine.IndexOf(termin), 0.0f);
                            }

                            instantiatedGameObjects.Add(g);
                        }
                    }
                } else {
                    GameObject g = Instantiate(prefabTermin);
                    g.transform.Find("TitelText").GetComponent<Text>().text = "Keine Termine";
                    g.transform.Find("StartTimeText").GetComponent<Text>().text = "";
                    g.transform.Find("EndTimeText").GetComponent<Text>().text = "";

                    g.transform.SetParent(dayCanvas.transform, false);

                    instantiatedGameObjects.Add(g);
                }
            }
        }
    }

    public void NewTermin() {
        saveButton.GetComponent<Button>().interactable = true;

        GameObject g = Instantiate(prefabNewTermin);
        Termin termin = new Termin();

        g.transform.SetParent(dayCanvas.transform, false);

        if (instantiatedGameObjects.Count == 1) {
            foreach (GameObject go in instantiatedGameObjects) {
                DestroyObject(go);
            }
            instantiatedGameObjects.Clear();

        } else if (instantiatedGameObjects.Count > 1) {
            foreach (GameObject go in instantiatedGameObjects) {
                DestroyObject(go);
            }
            instantiatedGameObjects.Clear();
            g.transform.position += new Vector3(0.0f, -80.0f * instantiatedGameObjects.Count, 0.0f);
        }

        g.transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = termin.GetColor();
        g.transform.Find("TitelText").GetComponent<Text>().text = termin.GetTitel().ToString();
        g.transform.Find("StartTimeText").GetComponent<Text>().text = termin.GetStart();
        g.transform.Find("EndTimeText").GetComponent<Text>().text = termin.GetEnd();


        instantiatedGameObjects.Add(g);

        if (dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable) {
            dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable = false;
        }

        gesturesManager.hasSwipedUp = true;
    }

    public void SaveTermin() {
        GameObject[] instantiated = instantiatedGameObjects.ToArray();
        Termin termin = null;
        if (!customColor) {
            termin = new Termin(instantiated[0].transform.Find("TitelText").GetComponent<Text>().text,
                instantiated[0].transform.Find("StartTimeText").GetComponent<Text>().text,
                instantiated[0].transform.Find("EndTimeText").GetComponent<Text>().text);
        } else {
            termin = new Termin(instantiated[0].transform.Find("TitelText").GetComponent<Text>().text,
                instantiated[0].transform.Find("StartTimeText").GetComponent<Text>().text,
                instantiated[0].transform.Find("EndTimeText").GetComponent<Text>().text,
                instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color);
        }

        foreach (Datum d in datenArray) {
            if (d.name.Equals(bName)) {
                d.addTermin(termin);
                break;
            }
        }

        if (!dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable) {
            dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable = true;
            saveButton.GetComponent<Button>().interactable = false;
        }

        gesturesManager.hasSwipedUp = false;
        customColor = false;
        Destroy(instantiated[0]);
        instantiatedGameObjects.Clear();
        instantiated = instantiatedGameObjects.ToArray();
        ShowTermine();
    }



    public int GetStartHours() {
        return startHours;
    }

    public int GetStartMinutes() {
        return startMinutes;
    }

    public int GetEndHours() {
        return endHours;
    }

    public int GetEndMinutes() {
        return endMinutes;
    }

    public void SetStartHours(int startHours) {
        this.startHours = startHours;
    }

    public void SetStartMinutes(int startMinutes) {
        this.startMinutes = startMinutes;
    }

    public void SetEndHours(int endHours) {
        this.endHours = endHours;
    }

    public void SetEndMinutes(int endMinutes) {
        this.endMinutes = endMinutes;
    }

    private void ClearTermine() {
        foreach (GameObject g in instantiatedGameObjects) {
            Destroy(g);
        }
        instantiatedGameObjects.Clear();
    }

    private class SortDaten : IComparer<Datum>
    {
        public int Compare(Datum x, Datum y) {
            int d1 = x.GetDate();
            int d2 = y.GetDate();

            return d1.CompareTo(d2);
        }
    }

    IEnumerator CloseButtonInteractable(float time) {
        yield return new WaitForSeconds(time);

        dayCanvas.transform.Find("CloseButtonPopUp").GetComponent<Button>().interactable = true;
    }
}
