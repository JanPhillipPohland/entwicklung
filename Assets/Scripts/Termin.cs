﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Termin {

    Titel titel;
    string start;
    string end;

    Color color;

    int startNumber;
    public enum Titel {
        TEST, ARZT, VL, UE, GEBURTSTAG, ARBEIT
    }

    public Termin() {
        this.titel = Titel.VL;
        this.start = "12:00";
        this.end = "13:00";

        this.color = Color.white;
    }

    public Termin(Titel titel, string start, string end) {
        this.titel = titel;
        this.start = start;
        this.end = end;

        this.color = SetColor();

        Debug.Log("StartSub: " + this.start.Substring(0, 2));
        this.startNumber = int.Parse(this.start.Substring(0, 2));
    }

    public Termin(string titel, string start, string end) {
        foreach (Titel t in Enum.GetValues(typeof(Titel))) {
            if (t.ToString().Equals(titel)) {
                this.titel = t;
            }
        }

        this.start = start;
        this.end = end;

        this.color = SetColor();
        this.startNumber = int.Parse(start.Substring(0, 2));
    }

    public Termin(string titel, string start, string end, Color color)
    {
        foreach (Titel t in Enum.GetValues(typeof(Titel)))
        {
            if (t.ToString().Equals(titel))
            {
                this.titel = t;
            }
        }

        this.start = start;
        this.end = end;

        this.color = color;
        this.startNumber = int.Parse(start.Substring(0, 2));
    }

    public void SetTitel(Titel titel) {
        this.titel = titel;
    }

    public void SetStart(string start) {
        this.start = start;
    }

    public void SetEnd(string end) {
        this.end = end;
    }

    public Titel GetTitel() {
        return this.titel;
    }

    public string GetStart() {
        return this.start;
    }

    public string GetEnd() {
        return this.end;
    }

    public Color GetColor() {
        return this.color;
    }

    public int GetStartNumber() {
        return this.startNumber;
    }

    Color SetColor() {
        if (this.titel == Titel.VL) {
            return new Color(0.0f, 1.0f, 171.0f / 255.0f, .2f);
        } else if (this.titel == Titel.UE) {
            return new Color(0.0f, 222.0f / 255.0f, 1.0f, .2f);
        } else if (this.titel == Titel.ARBEIT) {
            return new Color(1.0f, 206.0f / 255.0f, 41.0f / 255.0f, .2f);
        } else if (this.titel == Titel.ARZT) {
            return new Color(1.0f, 57.0f / 255.0f, 57.0f / 255.0f, .2f);
        } else if (this.titel == Titel.GEBURTSTAG) {
            return new Color(234.0f / 255.0f, 1.0f, 123.0f / 255.0f, .2f);
        } else {
            return Color.white;
        }
    }
}
