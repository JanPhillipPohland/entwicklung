﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeScript : MonoBehaviour
{


    public Text startHours;
    public Text startMinutes;

    public Text endHours;
    public Text endMinutes;

    public GameObject uiManager;

    int hoursCounter;
    int minutesCounter;

    /*GameObject hourUp;
    GameObject hourDown;
    GameObject minuteUp;
    GameObject minuteDown;
    */

    private void OnTriggerEnter(Collider other) {
        if (other.tag.Equals("Select")) {
            switch (name) {
                case "HourUp":
                    //InkrementHours(uiManager.GetComponent<UIManager>().GetStartHours()++, Starthours, "Start");
                    InkrementHours(startHours);
                    break;

                case "HourDown":
                    //DekrementHours(uiManager.GetComponent<UIManager>().GetStartHours()--, Starthours, "Start");
                    DekrementHours(startHours);
                    break;

                case "MinutesUp":
                    //InkrementMinutes(uiManager.GetComponent<UIManager>().GetStartMinutes() + 5, Startminutes, "Start");
                    InkrementMinutes(startMinutes);
                    break;

                case "MinutesDown":
                    //DekrementMinutes(uiManager.GetComponent<UIManager>().GetStartMinutes() - 5, Startminutes, "Start");
                    DekrementMinutes(startMinutes);
                    break;

                case "EndHourUp":
                    //InkrementHours(uiManager.GetComponent<UIManager>().GetEndHours()++, Endhours, "End");
                    InkrementHours(endHours);
                    break;

                case "EndHourDown":
                    //DekrementHours(uiManager.GetComponent<UIManager>().GetEndHours()++, Endhours, "End");
                    DekrementHours(endHours);
                    break;

                case "EndMinutesUp":
                    //InkrementMinutes(uiManager.GetComponent<UIManager>().GetEndMinutes() + 5, Endminutes, "End");
                    InkrementMinutes(endMinutes);
                    break;

                case "EndMinutesDown":
                    //DekrementMinutes(uiManager.GetComponent<UIManager>().GetEndMinutes() - 5, Endminutes, "End");
                    DekrementMinutes(endMinutes);
                    break;

            }
        }
    }

    void InkrementHours(Text textField) {
        if (textField == startHours) {
            if (uiManager.GetComponent<UIManager>().GetStartHours() == 23) {
                uiManager.GetComponent<UIManager>().SetStartHours(0);
            } else {
                uiManager.GetComponent<UIManager>().SetStartHours(uiManager.GetComponent<UIManager>().GetStartHours() + 1);
            }

            if (uiManager.GetComponent<UIManager>().GetStartHours() < 10) {
                startHours.text = "0" + uiManager.GetComponent<UIManager>().GetStartHours();
            } else {
                startHours.text = uiManager.GetComponent<UIManager>().GetStartHours().ToString();
            }
        } else if (textField == endHours) {
            if (uiManager.GetComponent<UIManager>().GetEndHours() == 23) {
                uiManager.GetComponent<UIManager>().SetEndHours(0);
            } else {
                uiManager.GetComponent<UIManager>().SetEndHours(uiManager.GetComponent<UIManager>().GetEndHours() + 1);
            }

            if (uiManager.GetComponent<UIManager>().GetEndHours() < 10) {
                endHours.text = "0" + uiManager.GetComponent<UIManager>().GetEndHours();
            } else {
                endHours.text = uiManager.GetComponent<UIManager>().GetEndHours().ToString();
            }
        }
    }

    void InkrementMinutes(Text textField) {
        if (textField == startMinutes) {
            if (uiManager.GetComponent<UIManager>().GetStartMinutes() == 55) {
                uiManager.GetComponent<UIManager>().SetStartMinutes(0);
            } else {
                uiManager.GetComponent<UIManager>().SetStartMinutes(uiManager.GetComponent<UIManager>().GetStartMinutes() + 5);
            }

            if (uiManager.GetComponent<UIManager>().GetStartMinutes() < 10) {
                startMinutes.text = "0" + uiManager.GetComponent<UIManager>().GetStartMinutes();
            } else {
                startMinutes.text = uiManager.GetComponent<UIManager>().GetStartMinutes().ToString();
            }
        } else if (textField == endMinutes) {
            if (uiManager.GetComponent<UIManager>().GetEndMinutes() == 55) {
                uiManager.GetComponent<UIManager>().SetEndMinutes(0);
            } else {
                uiManager.GetComponent<UIManager>().SetEndMinutes(uiManager.GetComponent<UIManager>().GetEndMinutes() + 5);
            }

            if (uiManager.GetComponent<UIManager>().GetEndMinutes() < 10) {
                endMinutes.text = "0" + uiManager.GetComponent<UIManager>().GetEndMinutes();
            } else {
                endMinutes.text = uiManager.GetComponent<UIManager>().GetEndMinutes().ToString();
            }
        }
    }

    void DekrementHours(Text textField) {
        if (textField == startHours) {
            if (uiManager.GetComponent<UIManager>().GetStartHours() == 0) {
                uiManager.GetComponent<UIManager>().SetStartHours(23);
            } else {
                uiManager.GetComponent<UIManager>().SetStartHours(uiManager.GetComponent<UIManager>().GetStartHours() - 1);
            }

            if (uiManager.GetComponent<UIManager>().GetStartHours() < 10) {
                startHours.text = "0" + uiManager.GetComponent<UIManager>().GetStartHours();
            } else {
                startHours.text = uiManager.GetComponent<UIManager>().GetStartHours().ToString();
            }
        } else if (textField = endHours) {
            if (uiManager.GetComponent<UIManager>().GetEndHours() == 0) {
                uiManager.GetComponent<UIManager>().SetEndHours(23);
            } else {
                uiManager.GetComponent<UIManager>().SetEndHours(uiManager.GetComponent<UIManager>().GetEndHours() - 1);
            }

            if (uiManager.GetComponent<UIManager>().GetEndHours() < 10) {
                endHours.text = "0" + uiManager.GetComponent<UIManager>().GetEndHours();
            } else {
                endHours.text = uiManager.GetComponent<UIManager>().GetEndHours().ToString();
            }
        }
    }

    void DekrementMinutes(Text textField) {
        if (textField == startMinutes) {
            if (uiManager.GetComponent<UIManager>().GetStartMinutes() == 0) {
                uiManager.GetComponent<UIManager>().SetStartMinutes(55);
            } else {
                uiManager.GetComponent<UIManager>().SetStartMinutes(uiManager.GetComponent<UIManager>().GetStartMinutes() - 5);
            }

            if (uiManager.GetComponent<UIManager>().GetStartMinutes() < 10) {
                startMinutes.text = "0" + uiManager.GetComponent<UIManager>().GetStartMinutes();
            } else {
                startMinutes.text = uiManager.GetComponent<UIManager>().GetStartMinutes().ToString();
            }
        } else if (textField == endMinutes) {
            if (uiManager.GetComponent<UIManager>().GetEndMinutes() == 0) {
                uiManager.GetComponent<UIManager>().SetEndMinutes(55);
            } else {
                uiManager.GetComponent<UIManager>().SetEndMinutes(uiManager.GetComponent<UIManager>().GetEndMinutes() - 5);
            }

            if (uiManager.GetComponent<UIManager>().GetEndMinutes() < 10) {
                endMinutes.text = "0" + uiManager.GetComponent<UIManager>().GetEndMinutes();
            } else {
                endMinutes.text = uiManager.GetComponent<UIManager>().GetEndMinutes().ToString();
            }
        }
    }



    /*

        public void InkrementHours(int hoursCounter, Text textfeld, string script)
        {
            if (hoursCounter == 24)
            {
                if (script.Equals("Start"))
                {
                    uiManager.GetComponent<UIManager>().GetStartHours() = 0;
                }
                else if (script.Equals("End"))
                {

                    uiManager.GetComponent<UIManager>().GetEndHours() = 0;

                }

                hoursCounter = 0;
            }

            if (hoursCounter < 10)
            {
                textfeld.text = "0" + hoursCounter;
            }
            else
            {
                textfeld.text = hoursCounter;
            }


        }

        public void DekrementHours(int hoursCounter, Text textfeld, string script)
        {


            if (hoursCounter == -1)
            {
                if (script.Equals("Start"))
                {
                    uiManager.GetComponent<UIManager>().GetStartHours() = 23;
                }
                else if (script.Equals("End"))
                {
                    uiManager.GetComponent<UIManager>().GetEndHours() = 23;

                }
                hoursCounter = 23;
            }

            if (hoursCounter < 10)
            {
                textfeld.text = "0" + hoursCounter;
            }
            else
            {
                textfeld.text = hoursCounter;


            }

        }


        public void InkrementMinutes(int minutesCounter, Text textfeld, string script)
        {


            if (minutesCounter == 60)
            {
                if (script.Equals("Start"))
                {
                    uiManager.GetComponent<UIManager>().GetStart() = 0;
                }
                else if (script.Equals("End"))
                {
                    TimeVariable.GetComponent<EndTimeVariables>().minutesCounter = 0;

                }

                minutesCounter = 0;
            }

            if (minutesCounter < 10)
            {
                textfeld.text = "0" + minutesCounter;
            }
            else
            {

                textfeld.text = minutesCounter;


            }


        }



        public void DekrementMinutes(int minutesCounter, Text textfeld, string script)
        {


            if (minutesCounter == -5)
            {

                if (script.Equals("Start"))
                {
                    TimeVariable.GetComponent<StartTimeVariables>().minutesCounter = 55;
                }
                else if (script.Equals("End"))
                {
                    TimeVariable.GetComponent<EndTimeVariables>().minutesCounter = 55;
                }

                minutesCounter = 59;
            }

            if (minutesCounter < 10)
            {
                textfeld.text = "0" + minutesCounter;
            }
            else
            {
                textfeld.text = minutesCounter;
            }

        }*/



}


/*
 
 1.Buttontext zwischen den Pfeilen verändert
 2.TimeScript eingebaut
 3.Den PfeilButton das TimeScript hinzugefügt
 4.Neues Gamobject TimeVariable erzeugt mit dem Skript TimeVariables 
    -> enthält globale Variable auf die all die Up/Down Buttons zugreifen
 5.Für jeden dieser Buttons im Inspector die TexxtKomponenten  + das GO Time



    //Problem da wir jeden Button das selbe Script geben hat jeder seinen eigenen Counter












    */
