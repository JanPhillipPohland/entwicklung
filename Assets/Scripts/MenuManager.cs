﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour
{

    UIManager uIManager;
    public Canvas handCanvas;
    public GameObject newDate;
    public GameObject color;
    public GameObject NewTitle;
    public GameObject Time;
    public GameObject newCustomColors;
    public GameObject[] presets;
    public GameObject[] times;
    public GameObject[] colors;

    public bool optionsOpened;
    public bool presetsOpened;
    public bool timeOpened;
    public bool colorsOpened;
    void Start() {
        uIManager = GameObject.Find("UIManager").GetComponent<UIManager>();
        optionsOpened = false;
        presetsOpened = false;
        timeOpened = false;
        colorsOpened = false;
    }

    public void ShowMainOptions() {
        newDate.SetActive(true);
        color.SetActive(true);
    }

    public void HideMainOptions() {
        newDate.SetActive(false);
        color.SetActive(false);
    }

    public void ShowNewOptions() {
        NewTitle.SetActive(true);
        Time.SetActive(true);
        newCustomColors.SetActive(true);
        optionsOpened = true;
    }

    public void HideNewOptions() {
        NewTitle.SetActive(false);
        Time.SetActive(false);
        newCustomColors.SetActive(false);
        optionsOpened = false;
    }

    public void ShowPresets() {
        foreach (GameObject g in presets) {
            g.SetActive(true);
        }

        presetsOpened = true;
    }

    public void HidePresets() {
        foreach (GameObject g in presets) {
            g.SetActive(false);
        }

        presetsOpened = false;
    }

    public void ShowTimeOptions() {
        foreach (GameObject g in times) {
            g.SetActive(true);
        }

        timeOpened = true;
    }

    public void HideTimeOptions() {
        foreach (GameObject g in times) {
            g.SetActive(false);
        }

        timeOpened = false;
    }
    public void ResetHandCanvas() {
        HideNewOptions();
        HidePresets();
        HideTimeOptions();
        HideCustomColors();
        ShowMainOptions();
    }

    public void ShowCustomColors() {
        foreach (GameObject g in colors) {
            g.SetActive(true);
        }

        colorsOpened = true;

    }

    public void HideCustomColors() {
        foreach (GameObject g in colors) {
            g.SetActive(false);
        }

        colorsOpened = false;
    }

    public void Wait(float time) {
        StartCoroutine(WaitTime(time));
    }

    IEnumerator WaitTime(float time) {
        yield return new WaitForSeconds(time);

    }
}
