﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Datum : MonoBehaviour {

    //public ArrayList termine = new ArrayList();
    public List<Termin> termine = new List<Termin>();
    int date;
    
	void Start () {
        date = int.Parse(name);

        if (name.Equals("01")) {
            addTermin(new Termin(Termin.Titel.VL, "11:00", "12:00"));
            addTermin(new Termin(Termin.Titel.UE, "12:15", "13:30"));
            addTermin(new Termin(Termin.Titel.ARBEIT, "15:00", "20:00"));
        }

        if (name.Equals("03")) {
            addTermin(new Termin(Termin.Titel.ARZT, "13:00", "16:00"));
            addTermin(new Termin(Termin.Titel.GEBURTSTAG, "17:00", "18:00"));
            addTermin(new Termin(Termin.Titel.TEST, "23:00", "00:00"));
        }
	}

    public void addTermin(Termin termin) {
        termine.Add(termin);
        foreach(Termin t in termine) {
            Debug.Log("TerminStartNumber: " + termin.GetStartNumber());
        }
        termine.Sort(new SortTermin());
    }

    public int GetDate() {
        return date;
    }

    private class SortTermin : IComparer<Termin>
    {
        public int Compare(Termin x, Termin y) {
            int t1 = x.GetStartNumber();
            int t2 = y.GetStartNumber();

            return t1.CompareTo(t2);
        }
    }

}
