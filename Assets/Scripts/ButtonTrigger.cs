﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTrigger : MonoBehaviour
{
    GameObject menuManager;
    GameObject uiManager;
    GameObject gesturesManager;
    GameObject[] instantiated;

    void Start() {
        menuManager = GameObject.Find("MenuManager");
        uiManager = GameObject.Find("UIManager");
        gesturesManager = GameObject.Find("Gestures");

        instantiated = uiManager.GetComponent<UIManager>().instantiatedGameObjects.ToArray();

    }

    private void OnTriggerEnter(Collider other) {
        instantiated = uiManager.GetComponent<UIManager>().instantiatedGameObjects.ToArray();
        if (other.tag.Equals("Select")) {
            switch (name) {
                case "NewDate":
                    GetComponentInChildren<Text>().color = Color.red;
                    Wait(0.5f, "Date");
                    uiManager.GetComponent<UIManager>().NewTermin();
                    break;
                case "NewTitel":
                    GetComponentInChildren<Text>().color = Color.red;
                    Wait(0.5f, "Titel");
                    break;
                case "VL":
                    GetComponentInChildren<Text>().color = Color.red;
                    SetPresetsText("VL");
                    break;
                case "UE":
                    GetComponentInChildren<Text>().color = Color.red;
                    SetPresetsText("UE");
                    break;
                case "ARBEIT":
                    GetComponentInChildren<Text>().color = Color.red;
                    SetPresetsText("ARBEIT");
                    break;
                case "GEBURTSTAG":
                    GetComponentInChildren<Text>().color = Color.red;
                    SetPresetsText("GEBURTSTAG");
                    break;
                case "ARZT":
                    GetComponentInChildren<Text>().color = Color.red;
                    SetPresetsText("ARZT");
                    break;
                case "NewTime":
                    GetComponentInChildren<Text>().color = Color.red;
                    if (menuManager.GetComponent<MenuManager>().optionsOpened) {
                        menuManager.GetComponent<MenuManager>().HideNewOptions();
                        menuManager.GetComponent<MenuManager>().ShowTimeOptions();
                    }
                    break;
                case "NewCustomColor":
                    GetComponentInChildren<Text>().color = Color.red;
                    uiManager.GetComponent<UIManager>().customColor = true;
                    if (menuManager.GetComponent<MenuManager>().optionsOpened) {
                        menuManager.GetComponent<MenuManager>().HideNewOptions();
                        menuManager.GetComponent<MenuManager>().ShowCustomColors();
                    }
                    break;
                case "ROT":
                    GetComponentInChildren<Text>().color = Color.red;
                    instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = Color.red;
                    break;
                case "GRÜN":
                    GetComponentInChildren<Text>().color = Color.red;
                    instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = Color.green;
                    break;
                case "BLAU":
                    GetComponentInChildren<Text>().color = Color.red;
                    instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = Color.blue;
                    break;
                case "GELB":
                    GetComponentInChildren<Text>().color = Color.red;
                    instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = Color.yellow;
                    break;
                case "WEISS":
                    GetComponentInChildren<Text>().color = Color.red;
                    instantiated[0].transform.Find("Background").GetComponent<UnityEngine.UI.Image>().color = Color.white;
                    break;
                case "Bestätigen":
                    GetComponentInChildren<Text>().color = Color.red;
                    if (menuManager.GetComponent<MenuManager>().presetsOpened || menuManager.GetComponent<MenuManager>().timeOpened || menuManager.GetComponent<MenuManager>().colorsOpened) {
                        menuManager.GetComponent<MenuManager>().HidePresets();
                        menuManager.GetComponent<MenuManager>().HideTimeOptions();
                        menuManager.GetComponent<MenuManager>().HideCustomColors();
                        menuManager.GetComponent<MenuManager>().ShowNewOptions();

                        SetTimeText();
                    }
                    break;

                default:
                    break;
            }
        }
    }

    private void SetPresetsText(string preset) {
        switch (preset) {
            case "VL":
                instantiated[0].transform.Find("TitelText").GetComponent<Text>().text = Termin.Titel.VL.ToString();
                break;
            case "UE":
                instantiated[0].transform.Find("TitelText").GetComponent<Text>().text = Termin.Titel.UE.ToString();
                break;
            case "ARBEIT":
                instantiated[0].transform.Find("TitelText").GetComponent<Text>().text = Termin.Titel.ARBEIT.ToString();
                break;
            case "GEBURTSTAG":
                instantiated[0].transform.Find("TitelText").GetComponent<Text>().text = Termin.Titel.GEBURTSTAG.ToString();
                break;
            case "ARZT":
                instantiated[0].transform.Find("TitelText").GetComponent<Text>().text = Termin.Titel.ARZT.ToString();
                break;
            default:
                break;
        }
    }

    private void SetTimeText() {
        string startTime = "";
        string endTime = "";
        if (uiManager.GetComponent<UIManager>().GetStartHours() < 10) {
            startTime = "0" + uiManager.GetComponent<UIManager>().GetStartHours() + ":";
        } else {
            startTime = "" + uiManager.GetComponent<UIManager>().GetStartHours() + ":";
        }

        if (uiManager.GetComponent<UIManager>().GetStartMinutes() < 10) {
            startTime += "0" + uiManager.GetComponent<UIManager>().GetStartMinutes();
        } else {
            startTime += "" + uiManager.GetComponent<UIManager>().GetStartMinutes();
        }

        if (uiManager.GetComponent<UIManager>().GetEndHours() < 10) {
            endTime = "0" + uiManager.GetComponent<UIManager>().GetEndHours() + ":";
        } else {
            endTime = "" + uiManager.GetComponent<UIManager>().GetEndHours() + ":";
        }

        if (uiManager.GetComponent<UIManager>().GetEndMinutes() < 10) {
            endTime += "0" + uiManager.GetComponent<UIManager>().GetEndMinutes();
        } else {
            endTime += "" + uiManager.GetComponent<UIManager>().GetEndMinutes();
        }

        instantiated[0].transform.Find("StartTimeText").GetComponent<Text>().text = startTime;
        instantiated[0].transform.Find("EndTimeText").GetComponent<Text>().text = endTime;
    }


    private void OnTriggerExit(Collider other) {
        if (other.tag.Equals("Select")) {
            Debug.Log("name: " + name);
            GetComponentInChildren<Text>().color = Color.black;
        }
    }

    public void Wait(float time, string options) {
        StartCoroutine(WaitTime(time, options));
    }

    IEnumerator WaitTime(float time, string options) {
        yield return new WaitForSeconds(time);
        if (options.Equals("Date")) {
            menuManager.GetComponent<MenuManager>().HideMainOptions();
            menuManager.GetComponent<MenuManager>().ShowNewOptions();
        } else if (options.Equals("Titel")) {
            menuManager.GetComponent<MenuManager>().HideNewOptions();
            menuManager.GetComponent<MenuManager>().ShowPresets();

        }

    }
}
